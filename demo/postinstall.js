var shell = require('shelljs');
var os = require('os');

var slash = (os.platform() === 'win32')?"\\\\":"/";
shell.exec(`node_modules${slash}.bin${slash}sequelize db:migrate --migrations-path services/migrations --config services/config/config.json`);
shell.exec(`node_modules${slash}.bin${slash}gulp build`);