'use strict';

const gulp = require('gulp');
const jshint = require('gulp-jshint');

var paths = gulp.paths;

gulp.task('lint', function () {


    return gulp.src(
        [paths.src + '/{app,components}/**/*.js',
        '!' + paths.src + '/{app,components}/**/*.spec.js',
        '!' + paths.src + '/{app,components}/**/*.mock.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'));
});