'use strict';

var gulp = require('gulp');

var paths = gulp.paths;

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', function () {
  return gulp.src([
    paths.src + '/{app,components}/**/*.html',
    paths.tmp + '/{app,components}/**/*.html'
  ])
    .pipe($.htmlmin({ collapseWhitespace: true }))
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'awesome-app'
    }))
    .pipe(gulp.dest(paths.tmp + '/partials/'));
});

gulp.task('html', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(paths.tmp + '/partials/templateCacheHtml.js', { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: paths.tmp + '/partials',
    addRootSlash: false
  };

  var htmlFilter = $.filter('*.html');
  var jsFilter = $.filter('**/*.js');
  var cssFilter = $.filter('**/*.css');
  var assets;

  return gulp.src(paths.tmp + '/serve/*.html')
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    .pipe($.rev())
    .pipe(jsFilter)
    .pipe($.ngAnnotate())
   // .pipe($.uglify({ preserveComments: $.uglifySaveLicense }))
    .pipe(jsFilter.restore())
    .pipe(cssFilter)
    .pipe($.csso())
    .pipe(cssFilter.restore())
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.htmlmin({ collapseWhitespace: true }))
    .pipe(htmlFilter.restore())
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size({ title: paths.dist + '/', showFiles: true }));
});

gulp.task('images', function () {
  return gulp.src(paths.src + '/assets/images/**/*')
    .pipe(gulp.dest(paths.dist + '/assets/images/'));
});

gulp.task('mail', function () {
  return gulp.src(paths.src + '/assets/mail/**/*')
    .pipe(gulp.dest(paths.dist + '/assets/mail/'));
});

gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles())
    .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    .pipe($.flatten())
    .pipe(gulp.dest(paths.dist + '/fonts/'));
});

gulp.task('misc', function () {
  return gulp.src(paths.src + '/**/*.ico')
    .pipe(gulp.dest(paths.dist + '/'));
});

/**
 * TODO: FIX This
 */

gulp.task('copyMCEPlugin', function () {
  return gulp.src('bower_components/tinymce/plugins/**')
    .pipe(gulp.dest(paths.dist + '/scripts/plugins/'));
});

gulp.task('copyMCETheme', function () {
  return gulp.src('bower_components/tinymce/themes/**')
    .pipe(gulp.dest(paths.dist + '/scripts/themes/'));
});

gulp.task('copyMCESkin', function () {
  return gulp.src('bower_components/tinymce/skins/**')
    .pipe(gulp.dest(paths.dist + '/scripts/skins/'));
});

gulp.task('copyMCE', ['copyMCEPlugin', 'copyMCETheme', 'copyMCESkin']);

gulp.task('clean', function (done) {
  $.del([paths.dist + '/', paths.tmp + '/'], done);
});

gulp.task('build', ['lint','test','html', 'mail','images', 'fonts', 'misc', 'copyMCE']);