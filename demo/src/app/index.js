'use strict';

angular.module('awesome-app', ['app'])
  .config(function ($locationProvider) {
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });
  })
  .run(function ($rootScope, $state) {

    $rootScope.$on('$stateChangeError',
      function (event, toState, toParams, fromState, fromParams, error) {
        if (!error.source || error.source !== 'checkAuth') {
          event.preventDefault();
          return;
        }

        if (error.status === 401) {
          // Implement a counter to detect number of retries
          var retryFunction = function () {
            $state.go(toState);
          };

          $rootScope.$broadcast('showAuthenticationDialog', { retryFunction: retryFunction });
          event.preventDefault();
          return;
        }

      });
  });