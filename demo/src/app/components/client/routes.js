'use strict';

angular.module('app').config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('client', {
            url: '/',
            controller: 'landingController',
            controllerAs: 'landing',
            templateUrl: 'app/components/client/landing/Landing.html'
        });
    // .state('client', {
    //     url: '',
    //     templateUrl: 'app/views/main-client.html',
    //     controller: 'AppController',
    //     abstract: true
    // })


    $urlRouterProvider.otherwise('/');
});