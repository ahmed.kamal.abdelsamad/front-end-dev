'use strict';
(function () {
    angular.module('app').service('landing', function () {

        var landingService = {
            getNavItems: getNavItems
        };

        function getNavItems() {
            return [{
                'title': 'Main'
            }, {
                'title': 'About Us'
            }, {
                'title': 'Links'
            }];
        }

        return landingService;
    });
})();