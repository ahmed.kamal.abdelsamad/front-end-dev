'use strict';
(function () {
    angular.module('app').controller('landingController', function (landing) {
        var vm = this;
        vm.navItems = landing.getNavItems();
    });
})();