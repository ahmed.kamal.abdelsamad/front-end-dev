describe('Landing Service', function () {

    var landing;

    beforeEach(function () {
        module('app');
    });

    beforeEach(inject(function (_landing_) {
        landing = _landing_;
    }));

    it('Should have a max of 3 nav elements', function () {
        var landingItems = landing.getNavItems();
        expect(landingItems.length).toBeLessThanOrEqual(3);
    });
});