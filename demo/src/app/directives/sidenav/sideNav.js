'use strict';

(function () {
    angular.module('app').directive('sideNav', function () {
        return {
            link: function (scope, element) {
                angular.element(element).sideNav();
            }
        };
    });
})();
