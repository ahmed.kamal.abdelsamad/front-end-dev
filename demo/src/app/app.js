(function () {
    'use strict';

    angular.module('app', ['ui.router']);
    angular.module('app').config(function () {

    }).run(function ($rootScope) {

        $rootScope.metadata = {
            title: 'Cool Website'
        };

    });
})();
